# Analitics listing fix

Some versions of OpenCart 2 can only have an analytics extension due to an [OpenCart bug](https://github.com/opencart/opencart/pull/5076).
This extension fixes that bug.

Remember to refresh your modifications after install.

You can download the extension from [here](https://gitlab.com/shoptimiza/opencart-fix-analytics-listing/uploads/d71d2a258d8ff7740becb77cfc5653c2/shoptimiza-fix-analytics-listing.ocmod.zip);
